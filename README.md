RumosDigitalBank

language: java version "11.0.7"; database: mysql 8.0

academic project

bank management system

client management - 
	new account;
	edit personal information;
	edit password;
	
card management -
	new card;
	cancel card;
	update password;

account management -
	withdraw;
	deposit;
	transfer;
