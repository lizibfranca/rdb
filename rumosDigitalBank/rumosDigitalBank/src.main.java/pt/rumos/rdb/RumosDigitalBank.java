package pt.rumos.rdb;

import java.util.Scanner;

import pt.rumos.rdb.adm.CustomerManagement;
import pt.rumos.rdb.adm.RumosManagement;
import pt.rumos.rdb.atm.RumosATM;
import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.view.RdbView;

public class RumosDigitalBank {
	private Scanner scanner = new Scanner(System.in);
	private RumosATM atm;
	private RumosManagement rdbAdm;
	private CustomerManagement customerAdm;
	private boolean end = false;
	

	public void run() throws DAOException {  
		
		do {
		RdbView.outputRdb();
		System.out.println("\nSelecione sua op��o");
		System.out.println("1 - Customer Management");
		System.out.println("2 - RDB Management");
		System.out.println("3 - ATM");
		System.out.println("0 - Exit\n");
		Integer option = scanner.nextInt();
		switch (option) {
		case 0:{
			end = true;
			break;
			}
		case 1:{
			customerAdm = new CustomerManagement();
			customerAdm.run();
			break;
			}
		case 2:{
			rdbAdm = new RumosManagement();
			rdbAdm.run();
			break;
			}
		case 3: {
				atm = new RumosATM();
				atm.run();
			break;
		}
		default:
			break;
		}
	}while(!end);
	}

}
