package pt.rumos.rdb.exception;

public class DAOException extends Exception {
	

	    private static final long serialVersionUID = 1L;

	    public DAOException(String string, Throwable throwable, boolean enableSuppression, boolean writableStackTrace) {
	        super(string, throwable, enableSuppression, writableStackTrace);
	    }

	    public DAOException(Throwable throwable) {
	        super(throwable);
	    }

	    public DAOException(String msg, Throwable throwable) {
	        super(msg, throwable);
	    }

	    public DAOException(String msg) {
	        super(msg);
	    }

	    public DAOException() {
	        super();
	    }
	    


}
