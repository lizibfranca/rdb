package pt.rumos.rdb.atm;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.Card;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.view.RdbView;

public class RumosATM {

	public void run() throws DAOException {

		String cardNumber = RdbView.inputCardNumber();
		if (CardService.validateCard(cardNumber)) {
			BankAccount account = BankAccountService.getAccount();
			String accountNumber = account.getAccountNumber();
			CreditCard creditCard = CardService.getCreditCardByCardNumber(cardNumber);
			DebitCard debitCard = CardService.getDebitCardByCardNumber(cardNumber);
			Card card;
			int option;
			if (creditCard != null) {
				card = creditCard;
				option = RdbView.outputAtmOptions("c");
			} else {
				card = debitCard;
				option = RdbView.outputAtmOptions("d");
			}

			switch (option) {
			case 0:
				break;
			case 1:
				BankAccountService.displayBalance(accountNumber);
				break;
			case 2:
				BankAccountService.deposit();
				break;
			case 3:
				String targetAccountNumber = RdbView.inputAccountNumber();
				Double amount = RdbView.inputAmount();
				BankAccountService.transference(accountNumber, targetAccountNumber, amount);
				break;
			case 4:
				BankAccountService.withdraw();
				break;
			case 5:
				BankAccountService.displayTransactionHistory(account.getIdAccount());
				break;
			case 6:
				CardService.updatePassword(card);
				break;
			case 7:
				//CardService.payCreditCardBill(card);
				break;
			case 8:
				//CardService.withdrawalPlafond(card);
				break;
			case 9:
				//CardService.payPlafond(card);
				break;
			default:
				RdbView.outputInvalidOption();
				break;
			}
		}
	}
}