package pt.rumos.rdb.service;

import java.sql.ResultSet;
import java.sql.SQLException;

import pt.rumos.rdb.model.Adress;

public class AdressService {

	private static Adress adress;

	public static Adress setAdress(ResultSet rs) throws SQLException {

		adress = new Adress();
		adress.setIdAdress(rs.getInt("id_adress"));
		adress.setStreet(rs.getString("street")); 
		adress.setPostalCode(rs.getString("zip_postal_code"));
		adress.setCity(rs.getString("city"));
		adress.setState(rs.getString("state_province"));
		adress.setCountry(rs.getString("country_region"));

		return adress;
	}

}
