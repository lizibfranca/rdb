package pt.rumos.rdb.service;

import java.time.LocalDate;
import java.util.List;

import pt.rumos.rdb.dao.AccountHasCustomerDao;
import pt.rumos.rdb.dao.AdressDao;
import pt.rumos.rdb.dao.BankAccountDao;
import pt.rumos.rdb.dao.CustomerDao;
import pt.rumos.rdb.dao.TransactionHistoryDao;
import pt.rumos.rdb.dao.WithdrawalControlDao;
import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.model.WithdrawalControl;
import pt.rumos.rdb.view.RdbView;

public class BankAccountService {

	private static BankAccount account;

	public static void registerNewAccount() throws Exception {
		CustomerService.registerNewCustomer();
		if (newAccount()) {
			BankAccountDao.insertNewAccount(account);
			WithdrawalControlDao.insertNewWithdralControl(account.getIdAccount());
			setCards(account.getCustomers().get(0));
			AccountHasCustomerDao.insertRelationshipAccountCustomer(account, CustomerService.getCustomer());
		} else {
			RdbView.outputProblem();
		}
	}

	private static boolean newAccount() {
		account = new BankAccount();
		account.setAccountNumber(generateAccNumber());
		account.setBalance(validateFirstDeposit());
		account.setCustomers(CustomerService.getCustomer());
		if (account.getBalance() < 50) {
			return false;
		}
		return true;
	}

	public static boolean checkTotalCreditCards(BankAccount account) throws DAOException {
		if (account.getCreditCardList().size() < 2) {
			return true;
		}
		return false;
	}

	public static boolean checkTotalDebitCards(BankAccount account) throws DAOException {
		if (account.getDebitCardList().size() < 5) {
			return true;
		}
		return false;
	}

	public static void setCards(Customer customer) throws DAOException {
		if (checkTotalCreditCards(account)) {
			CardService.registerNewCreditCard(account.getIdAccount(), customer.getIdCustomer());
		}
		if (checkTotalDebitCards(account)) {
			account.setDebitCard(CardService.registerNewDebitCard(account.getIdAccount(), customer.getIdCustomer()));
		}
	}

	public static String generateAccNumber() {
		String accountNumber;
		do {
			accountNumber = String.valueOf(((int) (Math.random() * (9999 - 1000) + 1)));
		} while (accountNumber.length() != 4);

		return accountNumber;
	}

	public static double validateFirstDeposit() {
		double firtsDeposit;
		do {
			firtsDeposit = RdbView.inputFirstDeposit();
		} while (firtsDeposit < 0);
		return firtsDeposit;
	}

	public static Double getBalance(String accountNumber) throws DAOException {
		Double balance = BankAccountDao.getBalance(accountNumber);
		return balance;
	}

	public static void displayBalance(String accountNumber) throws DAOException {
		Double balance = getBalance(accountNumber);
		if (balance != null) {
			RdbView.outputBalance(balance);
		}
	}

	public static void transference(String accountNumber, String targetAccountNumber, Double amount) throws DAOException {
		//String accountNumber = account.getAccountNumber();
		//String targetAccountNumber = RdbView.inputAccountNumber();
		if (accountNumber.equals(targetAccountNumber)) {
			RdbView.outputProblem();
		} else {
			//Double amount = RdbView.inputAmount();
			if (amount < 0) {
				throw new DAOException("Amount not allowed.");
			} else {
				Double currentBalance = BankAccountDao.getBalance(accountNumber);
				if (currentBalance >= amount) {
					if (targetAccountIsValid(targetAccountNumber)) {
						BankAccountDao.updateBalance(accountNumber, -amount);
						BankAccountDao.updateBalance(targetAccountNumber, amount);
						TransactionHistoryDao.insertNewTransaction(account.getIdAccount(),
								"transference sent to Account: " + targetAccountNumber, amount);
						int idTargetAccount = BankAccountDao.getAccountId(targetAccountNumber);
						TransactionHistoryDao.insertNewTransaction(idTargetAccount,
								"transference received from Account: " + accountNumber, amount);
						RdbView.outputDoneSuccessfully();
					}
				} else {
					RdbView.outputProblem();
				}
			}
		}
	}

	private static boolean targetAccountIsValid(String accountNumber) throws DAOException {
		BankAccount targetAccount = BankAccountDao.getAccountByAccountNumber(accountNumber);
		if (targetAccount != null) {
			return true;
		}
		return false;
	}

	public static void displayAccountByNif() throws DAOException {
		List<BankAccount> displayAccount = BankAccountDao.getAccountByNif(RdbView.inputNif());
		if (!displayAccount.isEmpty()) {
			System.out.println(displayAccount);
		} else {
			RdbView.outputNifNotFound();
		}
	}

	public static void displayAllAccounts() throws DAOException {
		System.out.println(BankAccountDao.getAllAccounts());

	}

	public static void checkIfCustomerHasMoreAccounts(String accNumber, Customer customer) throws DAOException {
		BankAccount accountToDelete = BankAccountDao.getAccountByAccountNumber(accNumber);
		int count = 0;
		List<BankAccount> accountList = BankAccountDao.getAllAccounts();
		for (BankAccount bankAccount : accountList) {
			for (Customer cust : bankAccount.getCustomers()) {
				if (cust.equals(customer)) {
					count++;
				}
			}
		}
		if (count > 1) {
			deleteAccount(accountToDelete);
		} else {
			deleteAccount(accountToDelete);
			for (Customer customerToDelete : accountToDelete.getCustomers()) {
				AdressDao.deleteAdress(customerToDelete.getAdress());
				CustomerDao.deleteCustomer(customerToDelete); //TODO: chamar o service
			}
		}

	}

	public static void deleteAccount(BankAccount accountToDelete) throws DAOException {
		if (accountToDelete.getCustomers().contains(CustomerService.getCustomer())) {
			int idAccount = accountToDelete.getIdAccount();
			AccountHasCustomerDao.deleteRelationship(idAccount);
			WithdrawalControlDao.deleteWithdralControl(idAccount);
			TransactionHistoryDao.deleteTransactionHistory(idAccount);
			for (Customer customerToDelete : accountToDelete.getCustomers()) {
				CardService.cancelDebitCard(accountToDelete.getDebitCardList(), idAccount,
						customerToDelete.getIdCustomer());
				CardService.cancelCreditCard(accountToDelete.getCreditCardList(), idAccount,
						customerToDelete.getIdCustomer());
			}
			BankAccountDao.deleteAccount(accountToDelete);
			RdbView.outputDoneSuccessfully();
		} else {
			RdbView.outputNifNotFound();
		}
	}

	public static BankAccount getAccountByAccountNumber(String accountNumber) throws DAOException {
		account = BankAccountDao.getAccountByAccountNumber(accountNumber);
		return account;
	}

	public static BankAccount getAccount() {
		return account;
	}

	public static boolean canSetANewCustomer() throws DAOException {
		getAccountByAccountNumber(RdbView.inputAccountNumber());
		if (account != null && account.getCustomers().size() < 5) {
			return true;
		}
		return false;
	}

/*	public static void accountManager() throws DAOException {
		// TODO: RETIRAR O MENU*********************************
		// NADA DE SWITCH NA CAMADA DE NEGOCIO
		// INICIALIZAR AS VARIAVEIS EM OUTRA CLASSE TB
		String accountNumber = RdbView.inputAccountNumber();
		account = BankAccountDao.getAccountByAccountNumber(accountNumber);
		if ((account != null)) {
			String nif = RdbView.inputNif();
			Customer customer = CustomerService.getCustomerByNif(nif);
			if (account.getCustomers().contains(customer)) {
			//	if (CustomerService.checkCustomersPassword()) {

					int option = RdbView.inputAccountManager();

					switch (option) {
					case 0: {
						break;
					}
					case 1: {
						displayBalance(accountNumber); // TODO: Retirar o parametro, como em transferencia?
						break;
					}
					case 2: {
						transference();
						break;
					}
					case 3: {
						displayTransactionHistory(account.getIdAccount());
						break;
					}
					case 4: {
						if (account.getDebitCardList().isEmpty()) {
							CardService.registerNewDebitCard(account.getIdAccount(), customer.getIdCustomer());
							RdbView.outputDoneSuccessfully();
						} else {
							for (DebitCard debitCard : account.getDebitCardList()) {
								if (checkTotalDebitCards() && !(debitCard.getCustomer().equals(customer))) {
									CardService.registerNewDebitCard(account.getIdAccount(), customer.getIdCustomer());
									RdbView.outputDoneSuccessfully();
								} else {
									RdbView.outputProblem();
								}
							}
						}
						break;
					}
					case 5: {
						if (account.getCreditCardList().isEmpty()) {
							CardService.registerNewCreditCard(account.getIdAccount(), customer.getIdCustomer());
							RdbView.outputDoneSuccessfully();
						} else {
							for (CreditCard creditCard : account.getCreditCardList()) {
								if (checkTotalCreditCards() && !(creditCard.getCustomer().equals(customer))) {
									CardService.registerNewCreditCard(account.getIdAccount(), customer.getIdCustomer());
									RdbView.outputDoneSuccessfully();
								} else {
									RdbView.outputProblem();
								}
							}
						}
						break;
					}
					case 6: {
						CardService.cancelDebitCard(account.getDebitCardList(), account.getIdAccount(),
								customer.getIdCustomer());
						break;
					}
					case 7: {
						CardService.cancelCreditCard(account.getCreditCardList(), account.getIdAccount(),
								customer.getIdCustomer());
						break;
					}
					default:
						RdbView.outputInvalidOption();
						break;
					}

			//	} else {
			//		RdbView.outputIncorrectPass();
			//	}

			} else {
				RdbView.outputNifNotFound();
			}
		} else {
			RdbView.outputAccountNotFound();
		}

	} */

	public static void displayTransactionHistory(Integer idAccount) throws DAOException {
		System.out.println(TransactionHistoryDao.getTransactionHistory(idAccount));

	}

	public static void getAccountByCardNumber(String cardNumber) throws DAOException {
		account = BankAccountDao.getAccountByCardNumber(cardNumber);

	}

	public static void deposit() throws DAOException {
		Double amount = RdbView.inputAmount();
		if (amount > 0) {
			BankAccountDao.updateBalance(account.getAccountNumber(), amount);
			TransactionHistoryDao.insertNewTransaction(account.getIdAccount(), "deposit ", amount);
			RdbView.outputDoneSuccessfully();
		} else {
			throw new DAOException("Amount not allowed.");
		}

	}

	public static void withdraw() throws DAOException {
		Double amount = RdbView.inputAmount();
		String accountNumber = account.getAccountNumber();
		Double balance = getBalance(accountNumber);
		if (balance >= amount) {
			if (amount <= 200) {
				if (canWithdraw(account.getIdAccount(), amount)) {
					BankAccountDao.updateBalance(account.getAccountNumber(), -amount);
					TransactionHistoryDao.insertNewTransaction(account.getIdAccount(), "withdraw", amount);
					RdbView.outputDoneSuccessfully();
				} else {
					RdbView.outputDailyLimitExceeded();
				}
			} else {
				RdbView.outputDailyLimitExceeded();
			}
		} else {
			RdbView.outputInsufficientFunds();
		}
	}

	private static boolean canWithdraw(Integer idAccount, Double amount) throws DAOException {
		WithdrawalControl withdrawalControl = WithdrawalControlDao.getTotalWithdrawalDay(idAccount);
		Double totalWithdralDay = withdrawalControl.getSumWithdrawalDay();
		LocalDate date = withdrawalControl.getDate();
		LocalDate dateNow = LocalDate.now();
		if (date.isEqual(dateNow) && (totalWithdralDay + amount) <= 400) {
			amount += totalWithdralDay;
			WithdrawalControlDao.updateTotalWithdrawal(idAccount, amount);
			return true;
		} else if (date.isEqual(dateNow) && (totalWithdralDay + amount) > 400) {
			return false;
		} else {
			WithdrawalControlDao.resetTotalWithdrawal(idAccount, amount);
			return true;
		}

	}

}
