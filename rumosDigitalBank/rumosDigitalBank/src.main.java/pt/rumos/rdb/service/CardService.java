package pt.rumos.rdb.service;

import java.util.List;

import pt.rumos.rdb.dao.CardDao;
import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Card;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.view.RdbView;

public class CardService {

	private static DebitCard debitCard;
	private static CreditCard creditCard;

	public static DebitCard registerNewDebitCard(Integer idAccount, Integer idCustomer) throws DAOException {
		// TODO: RENOMEAR ESTES MEDTODOS
		if (RdbView.inputAddNewDebitCard()) {
			newDebitCard(idAccount, idCustomer);
			CardDao.insertNewDebitCard(debitCard);
			return debitCard;
		}
		return null;
	}

	private static void newDebitCard(Integer idAccount, Integer idCustomer) {
		debitCard = new DebitCard();
		debitCard.setCardNumber(generateCardNumber());
		debitCard.setAccountIdAccount(idAccount);
		debitCard.setCustomerIdCustomer(idCustomer);
		debitCard.setPassword("2121");
	}

	public static CreditCard registerNewCreditCard(Integer idAccount, Integer idCustomer) throws DAOException {
		if (RdbView.inputAddNewCreditCard()) {
			newCreditCard(idAccount, idCustomer);
			CardDao.insertNewCreditCard(creditCard);
			return creditCard;
		}
		return null;
	}

	private static void newCreditCard(Integer idAccount, Integer idCustomer) {
		creditCard = new CreditCard();
		creditCard.setCardNumber(generateCardNumber());
		creditCard.setAccountIdAccount(idAccount);
		creditCard.setCustomerIdCustomer(idCustomer);
		creditCard.setPassword("2121");
	}

	public static String generateCardNumber() {
		String cardNumber;
		do {
			cardNumber = String.valueOf(((int) (Math.random() * (9999 - 1000) + 1)));
		} while (cardNumber.length() < 4);

		return cardNumber;
	}

	public static DebitCard getDebitCard() {
		return debitCard;
	}

	public static CreditCard getCreditCard() {
		return creditCard;
	}

	public static void cancelDebitCard(List<DebitCard> debitCardList, Integer idAccount, Integer idCustomer)
			throws DAOException {
		for (DebitCard debitCardToDelete : debitCardList) {
			if (debitCardToDelete.getAccountIdAccount() == idAccount
					&& debitCardToDelete.getCustomerIdCustomer() == idCustomer) {
				debitCard = debitCardToDelete;
			}
		}
		if (!(debitCard == null)) {
			CardDao.deleteCard(debitCard);
			// RdbView.outputDoneSuccessfully();
		}

	}

	public static void cancelCreditCard(List<CreditCard> creditCardList, Integer idAccount, Integer idCustomer)
			throws DAOException {
		for (CreditCard creditCardToDelete : creditCardList) {
			if (creditCardToDelete.getAccountIdAccount() == idAccount
					&& creditCardToDelete.getCustomerIdCustomer() == idCustomer) {
				creditCard = creditCardToDelete;
			}
		}
		if (!(creditCard == null)) {
			CardDao.deleteCard(creditCard);
			// RdbView.outputDoneSuccessfully();
		}
	}

	public static boolean validateCard(String cardNumber) throws DAOException {
		Card card = getCardByCardNumber(cardNumber);
		if (card != null && checkCardsPassword(card)) {
			if (card.getIsFirstUse()) {
				updatePassword(card);
			}
			BankAccountService.getAccountByCardNumber(cardNumber);
			return true;
		} else {
			RdbView.outputProblem();
			return false;
		}
	}
	
	public static Card getCardByCardNumber(String cardNumber) throws DAOException {
		CreditCard creditCard = getCreditCardByCardNumber(cardNumber);
		DebitCard debitCard = getDebitCardByCardNumber(cardNumber);
		Card card = null;
		if (creditCard != null) {
			card = creditCard;
		} else if(debitCard  != null) {
			card = debitCard;
		}
		return card;
	}

	public static CreditCard getCreditCardByCardNumber(String cardNumber) throws DAOException {
		CreditCard creditCard = CardDao.getCreditCardByCardNumber(cardNumber);
		return creditCard;
	}
	
	public static DebitCard getDebitCardByCardNumber(String cardNumber) throws DAOException {
		DebitCard debitCard = CardDao.getDebitCardByCardNumber(cardNumber);
		return debitCard;
	}

	public static boolean checkCardsPassword(Card card) throws DAOException {
		String inputPassword = RdbView.inputPassword();
		if (inputPassword.equals(card.getPassword())) {
			return true;
		} else {
			RdbView.outputIncorrectPass();
			return false;
		}

	}

	public static void updatePassword(Card card) throws DAOException {
		String newPassword;
		do {
			newPassword = RdbView.inputNewPassword();
		} while (newPassword.equals(card.getPassword()));

		CardDao.updatePassword(card.getCardNumber(), newPassword);
		RdbView.outputDoneSuccessfully();
	}
}
