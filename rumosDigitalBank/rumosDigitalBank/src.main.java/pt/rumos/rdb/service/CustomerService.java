package pt.rumos.rdb.service;

import java.time.LocalDate;
import java.time.Period;

import pt.rumos.rdb.dao.AccountHasCustomerDao;
import pt.rumos.rdb.dao.AdressDao;
import pt.rumos.rdb.dao.CustomerDao;
import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Adress;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.view.RdbView;

public class CustomerService {

	private static Customer customer;
	private static String nif;

	public static boolean isNifAlreadyRegistered() throws DAOException {
		nif = RdbView.inputNif();
		customer = CustomerDao.getCustomerByNif(nif);
		if (customer == null) {
			return false;
		}
		return true;
	}

	public static void registerNewCustomer() throws Exception {
		if (!isNifAlreadyRegistered()) {
			newCustomer(true);
		}
	} 

	public static void newCustomer(boolean isTheOwner) throws DAOException {
		// TODO: COLOAR ISTO NA CAMADA ADM(?)
			customer = new Customer();
			if (nif != null) {
				customer.setNif(nif);
			} else {
				customer.setNif(RdbView.inputNif());
			}
			customer.setName(RdbView.inputFullName());
			customer.setTheOwner(isTheOwner);
			if (customer.isTheOwner()) {
				validateAge(RdbView.inputBirthdate());
			} else {
				customer.setBirthdate(RdbView.inputBirthdate());
			}
			customer.setPassword("2121"); 
			customer.setTelephone(RdbView.inputTelephone());
			customer.setCellphone(RdbView.inputCellphone());
			customer.setEmail(RdbView.inputEmail());
			customer.setOccupation(RdbView.inputOccupation());
			customer.setAdress(new Adress(RdbView.inputStreet() + "; ", RdbView.inputCity()+ " ;", RdbView.inputPostalCode() 
			+ " ;", RdbView.inputState() + "; ", RdbView.inputCountry()));
			
			insertNewCustomer(customer);
			insertCustomersAdress(customer);
	} 
	
	public static void validateAge(LocalDate inputBirthdate) throws DAOException {
		Period age = inputBirthdate.until(LocalDate.now());
		if(age.getYears() < 18) {
			throw new DAOException("The primary account holder must be over 18 years old."); 
		} else {
			customer.setBirthdate(inputBirthdate);
		}
	}


	public static void newCustomerCoOwner() throws Exception {
		if (BankAccountService.canSetANewCustomer()) {
			newCustomer(false);
			BankAccountService.setCards(customer);
			AccountHasCustomerDao.insertRelationshipAccountCustomer(BankAccountService.getAccount(), customer);
		} else {
			RdbView.outputProblem();
		}
	}

	public static int getCustomerId() {
		return customer.getIdCustomer();
	}

	public static Customer getCustomer() {
		return customer;
	}

	public static void setCustomer(Customer setCustomer) {
		customer = setCustomer;
	}

/*	public static void editCustomer() throws DAOException {
		if (isNifAlreadyRegistered()) {
			if(checkCustomersPassword()) {
			int option = RdbView.outputEditCustomer();

				switch (option) {
				case 0: {
					break;
				}
				case 1: {
					customer.setName(RdbView.inputFullName());
					break;
				}
				case 2: {
					customer.setPassword(RdbView.inputNewPassword());
					customer.setTelephone(RdbView.inputTelephone());
					break;
				}
				case 3: {
					customer.setCellphone(RdbView.inputCellphone());
					break;
				}
				case 4: {
					customer.setEmail(RdbView.inputEmail());
					break;
				}
				case 5: {
					customer.setOccupation(RdbView.inputOccupation());
					break;
				}
				default:
					RdbView.outputInvalidOption();
					break;
				}
				//TODO: O UPDATE RODA MESMO NO DEFAULT
			CustomerDao.updateCustomer(customer);
		} else {
			RdbView.outputIncorrectPass();
		} 
		} else {
			RdbView.outputNifNotFound();
		}
	} */

	public static void deleteCustomer() throws DAOException {
		// TODO: VERIFICAR PENDENCIAS: SALDO, DEBITO...
		if (isNifAlreadyRegistered()) {

			if (customer.isTheOwner()) {
				BankAccountService.checkIfCustomerHasMoreAccounts(RdbView.inputAccountNumber(), customer);
				
			} else {
				int idAccount = BankAccountService.getAccountByAccountNumber(RdbView.inputAccountNumber()).getIdAccount();
				AccountHasCustomerDao.deleteRelationship(idAccount, customer.getIdCustomer());
				AdressDao.deleteAdress(customer.getAdress());
				CardService.cancelDebitCard(BankAccountService.getAccount().getDebitCardList(), idAccount, customer.getIdCustomer());
				CardService.cancelCreditCard(BankAccountService.getAccount().getCreditCardList(), idAccount, customer.getIdCustomer());
				CustomerDao.deleteCustomer(customer);
				RdbView.outputDoneSuccessfully();
			}
		} else {
			RdbView.outputNifNotFound();
		}
	}
	
	public static boolean checkCustomersPassword() throws DAOException {
		String inputPassword = RdbView.inputPassword();
		if (inputPassword.equals(customer.getPassword())) {
			return true;
		}
		return false;
	} 

	public static Customer getCustomerByNif(String nif) throws DAOException {
		customer = CustomerDao.getCustomerByNif(nif);
		return  customer;
	}
	
	public static void setName(String name) {
		customer.setName(name);
	}

	public static void setPassword(String password) {
		customer.setPassword(password);
	}

	public static void setTelephone(String telephone) {
		customer.setTelephone(telephone);
	}

	public static void setCellphone(String cellphone) {
		customer.setCellphone(cellphone);		
	}

	public static void setEmail(String email) {
		customer.setEmail(email);
	}

	public static void setOccupation(String occupation) {
		customer.setOccupation(occupation);
	}
	
	public static void updateCustomer() throws DAOException {
		CustomerDao.updateCustomer(customer);
	}

	public static void insertNewCustomer(Customer customer2) throws DAOException {
		CustomerDao.insertNewCustomer(customer);
	}

	public static void insertCustomersAdress(Customer customer) throws DAOException {
		AdressDao.insertCustomersAdress(customer);
	}
}
