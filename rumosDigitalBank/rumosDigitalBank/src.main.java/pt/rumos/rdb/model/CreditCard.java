package pt.rumos.rdb.model;


public class CreditCard extends Card{
	
	
	private Double plafond = 500.0;
	
	public CreditCard() {
		super();
	}
	
	public CreditCard(String cardNumber) {
		super(cardNumber);
	}

	public Double getPlafond() {
		return plafond;
	}

	public void setPlafond(Double plafond) {
		this.plafond = plafond;
	}
	
	@Override
	public String toString() {
		return "CreditCard [ "
				+ "Card Number: " + super.getCardNumber()   + "; "
				+ "Customer: " + getCustomer().getName() + " ] \r\n";
	}



}
