package pt.rumos.rdb.model;

public class DebitCard extends Card{
	
	public DebitCard() {
		super();
	}
	
	public DebitCard(String cardNumber) {
		super(cardNumber);
	}
	
	@Override
	public String toString() {
		return "DebitCard [ "
				+ "Card Number: " + super.getCardNumber()  + "; "
				+ "Customer: " + getCustomer().getName() + " ] \r\n";
	}

	
	
}
