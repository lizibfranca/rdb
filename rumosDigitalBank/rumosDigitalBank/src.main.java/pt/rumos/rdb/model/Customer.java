package pt.rumos.rdb.model;

import java.time.LocalDate;
import java.util.Objects;

public class Customer {

	private Integer idCustomer;
	private String nif;
	private String password;
	private String name;
	private LocalDate birthdate;
	private String telephone;
	private String cellphone;
	private String email;
	private String occupation;
	private Adress adress;
	private Boolean isTheOwner; //TODO:
	
	public Customer() {
		super();
	}
	
	public Customer (String nif) {
		setNif(nif);
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	} 

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LocalDate getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(LocalDate birthdate) {
		this.birthdate = birthdate;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getCellphone() {
		return cellphone;
	}

	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public Adress getAdress() {
		return adress;
	}
	
	public void setAdress(Adress adress) {
		this.adress = adress;
	}
	
	public Boolean isTheOwner() {
		return isTheOwner;
	}

	public void setTheOwner(Boolean isTheOwner) {
		this.isTheOwner = isTheOwner;
	}
	
	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nif == null) ? 0 : nif.hashCode());
		return result;
	}

	//TODO: VERIFICAR OVERRIDE EQUALS
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Customer other = (Customer) obj;
		return Objects.equals(nif, other.nif);
	}
	
	@Override
	public String toString() {
		return " \r\n Customer [ "
				+ "NIF: " + nif + "\r\n"
				+ "Name: " + name + "\r\n"
				+ "Birthdate: " + birthdate + "\r\n"
				+ "Occupation: " + occupation + "\r\n"
				+ "Contacts: " + telephone + "; " + cellphone + "; "+ email + "\r\n"
				+ "Adress: " + adress + "\r\n" 
				+ "Is the Owner: " + isTheOwner  + " ]" + "\r\n" ;
	}

}
