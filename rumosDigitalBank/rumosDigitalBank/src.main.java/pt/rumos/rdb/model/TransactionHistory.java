package pt.rumos.rdb.model;

import java.time.LocalDate;

public class TransactionHistory {
	
	private Integer idTransactionHistory;
	private Integer idAccount;
	private String descricao;
	private Double amount;
	private LocalDate date;  
	
	public Integer getIdTransactionHistory() {
		return idTransactionHistory;
	}
	public void setIdTransactionHistory(Integer idTransactionHistory) {
		this.idTransactionHistory = idTransactionHistory;
	}
	public Integer getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(Integer idAccount) {
		this.idAccount = idAccount;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "\n[descricao: " + descricao + ", amount: " + amount + " �, date: " + date + "]\n";
	}
	
	
	
	
	

}
