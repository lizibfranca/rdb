package pt.rumos.rdb.model;

import java.time.LocalDate;

public class DetailedStatement {
	
	private Integer idCreditCarBill;
	private Integer cardIdcard;
	private String descricao;
	private Double amount;
	private LocalDate date;
	private String status;
	
	public Integer getIdCreditCarBill() {
		return idCreditCarBill;
	}
	public void setIdCreditCarBill(Integer idCreditCarBill) {
		this.idCreditCarBill = idCreditCarBill;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public Integer getCardIdcard() {
		return cardIdcard;
	}
	public void setCardIdcard(Integer cardIdcard) {
		this.cardIdcard = cardIdcard;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	

}
