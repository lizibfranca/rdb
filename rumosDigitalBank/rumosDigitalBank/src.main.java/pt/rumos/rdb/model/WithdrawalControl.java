package pt.rumos.rdb.model;

import java.time.LocalDate;

public class WithdrawalControl {
	
	private Integer idWithdrawalControl;
	private Integer idAccount;
	private Double sumWithdrawalDay;
	private LocalDate date;
	
	public Integer getIdWithdrawalControl() {
		return idWithdrawalControl;
	}
	public void setIdWithdrawalControl(Integer idWithdrawalControl) {
		this.idWithdrawalControl = idWithdrawalControl;
	}
	public Integer getIdAccount() {
		return idAccount;
	}
	public void setIdAccount(Integer idAccount) {
		this.idAccount = idAccount;
	}
	public Double getSumWithdrawalDay() {
		return sumWithdrawalDay;
	}
	public void setSumWithdrawalDay(Double sumWithdrawalDay) {
		this.sumWithdrawalDay = sumWithdrawalDay;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	
	

}
