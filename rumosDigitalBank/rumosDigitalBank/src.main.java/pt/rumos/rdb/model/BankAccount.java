package pt.rumos.rdb.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BankAccount {

	private Integer idAccount;
	private String accountNumber;
	private List<Customer> customers = new ArrayList<Customer>();
	private Double balance = 0.0;
	private List<DebitCard> debitCardList = new ArrayList<DebitCard>();
	private List<CreditCard> creditCardList= new ArrayList<CreditCard>();
	private Double sumWithdrawDay;
	
	public BankAccount() {
		super();
	}
	
	public BankAccount(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	public BankAccount(Customer customer) {
		this.customers.add(customer);
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public List<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(Customer customer) {
		this.customers.add(customer);
	}
	
	public void deleteCustomers(Customer customer) {
		this.customers.remove(customer);
	}

	public Double getBalance() {
		return balance;
	}

	public void setBalance(Double balance) {
		this.balance = this.balance + balance;
	}
	
	public List<DebitCard> getDebitCardList() {
		return debitCardList;
	}

	public void setDebitCard(DebitCard debitCard) {
		debitCardList.add(debitCard);
	}

	public List<CreditCard> getCreditCardList() {
		return creditCardList;
	}

	public void setCreditCard(CreditCard creditCard) {
		creditCardList.add(creditCard);
	}
	
	public Integer getIdAccount() {
		return idAccount;
	}

	public void setIdAccount(Integer idAccount) {
		this.idAccount = idAccount;
	}
	
	public Double getSumWithdrawDay() {
		return sumWithdrawDay;
	}

	public void setSumWithdrawDay(Double sumWithdrawDay) {
		this.sumWithdrawDay = sumWithdrawDay;
	}

	@Override
	public int hashCode() {
		return Objects.hash(accountNumber, balance, customers);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		BankAccount other = (BankAccount) obj;
		return Objects.equals(accountNumber, other.accountNumber);
	}

	@Override
	public String toString() {
		return "{BankAccount [ "
				+ "Account Number: " + accountNumber + "\r\n" 
				+ "Balance: " + balance + "� \r\n" 
				+ "Customers: " + customers + "\r\n" 
				+ "DebitCards: " + debitCardList + "\r\n" 
				+ "CreditCards: " + creditCardList + " ]} \r\n";
	}

}

