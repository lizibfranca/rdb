package pt.rumos.rdb.model;

import java.util.Objects;

public class Card {

	private Integer idCard;
	private String cardNumber;
	private String password;
	private Double dailyExtract;
	private Boolean firstuse = true;
	private Customer customer;
	private Integer accountIdAccount;
	private Integer customerIdCustomer;
	private Boolean isFirstUse;

	public Card() {}

	public Card(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Double getDailyExtract() {
		return dailyExtract;
	}

	public void setDailyExtract(Double dailyExtract) {
		this.dailyExtract = dailyExtract;
	}

	public Boolean isFirstuse() {
		return firstuse;
	}

	public void setFirstuse(boolean firstuse) {
		this.firstuse = firstuse;
	}
	
	public Integer getIdCard() {
		return idCard;
	}

	public void setIdCard(Integer idCard) {
		this.idCard = idCard;
	}
	
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	public Integer getAccountIdAccount() {
		return accountIdAccount;
	}

	public void setAccountIdAccount(Integer accountIdAccount) {
		this.accountIdAccount = accountIdAccount;
	}

	public Integer getCustomerIdCustomer() {
		return customerIdCustomer;
	}

	public void setCustomerIdCustomer(Integer customerIdCustomer) {
		this.customerIdCustomer = customerIdCustomer;
	}
	
	public Boolean getIsFirstUse() {
		return isFirstUse;
	}

	public void setIsFirstUse(Boolean isFirstUse) {
		this.isFirstUse = isFirstUse;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(cardNumber, dailyExtract, firstuse, password);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Card other = (Card) obj;
		return Objects.equals(cardNumber, other.cardNumber);
	}
}
