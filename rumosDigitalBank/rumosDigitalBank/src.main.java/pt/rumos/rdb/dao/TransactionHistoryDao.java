package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.TransactionHistory;
import pt.rumos.rdb.util.JdbcConnection;

public class TransactionHistoryDao {
	// TODO: revisar o logger em todas as classes dao e revisar a excption
	private static final Logger LOGGER = Logger.getLogger(TransactionHistoryDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;

	public static void insertNewTransaction(Integer idAcc, String descricao, Double amount)
			throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection
					.prepareCall("INSERT INTO transaction_history (account_id_acc, descricao, amount, date) "
							+ "VALUES (?, ?, ?, NOW());");
			st.setInt(1, idAcc);
			st.setString(2, descricao);
			st.setDouble(3, amount);

			st.execute();

			JdbcConnection.closeConnection(connection);

			} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewTransaction.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewTransaction.", ex);
		}

	}
	
	public static List<TransactionHistory> getTransactionHistory(Integer idAccount) throws DAOException {
		 List<TransactionHistory> transactionHistoryList = new ArrayList<TransactionHistory>();
		 //TODO: filtrar periodo
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT id_transaction_history, account_id_acc, descricao, "
					+ "amount, date FROM transaction_history WHERE account_id_acc = " + idAccount + ";");

			if (st.execute()) {
				rs = st.getResultSet();
				while (rs.next()) {
					transactionHistoryList.add(fillNewTransaction(rs));
				}
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getTransactionHistory.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getTransactionHistory.", ex);
		}

		return transactionHistoryList;

	}

	private static TransactionHistory fillNewTransaction(ResultSet rs) throws SQLException {

		TransactionHistory transactionHistory = new TransactionHistory();

		transactionHistory.setIdTransactionHistory(rs.getInt("id_transaction_history"));
		transactionHistory.setIdAccount(rs.getInt("account_id_acc"));
		transactionHistory.setAmount(rs.getDouble("amount"));
		transactionHistory.setDescricao(rs.getString("descricao"));
		transactionHistory.setDate(convertToLocalDateViaSqlDate(rs.getDate("date")));

		return transactionHistory;

	}
	
	public static void deleteTransactionHistory(int idAcc) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM transaction_history WHERE account_id_acc = " 
			+ idAcc + ";");
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteTransactionHistory.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteTransactionHistory.", ex);
		}
	}

	public static LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
		return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
	}

}
