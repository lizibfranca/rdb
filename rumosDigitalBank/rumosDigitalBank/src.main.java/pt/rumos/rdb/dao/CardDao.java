package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Card;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.util.JdbcConnection;

public class CardDao {

	private static final Logger LOGGER = Logger.getLogger(CardDao.class.getName()); // TODO: CORRIGIR
	private static Connection connection = null;
	private static CallableStatement st;
	// private static ResultSet rs;
	private static DebitCard debitCard;
	private static CreditCard creditCard;
	private static List<DebitCard> debitCardList = new ArrayList<DebitCard>();
	private static List<CreditCard> creditCardList = new ArrayList<CreditCard>();
	private static Card card;

	public static void insertNewDebitCard(DebitCard debitCard) throws DAOException {
		// TODO: REPETI��O DE C�DIGO<<<<<<<<<<<
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall(
					"INSERT INTO card (card_number, type, account_id_acc, customer_id_cust) VALUES ( ?, ?, ?, ?);");

			st.setString(1, CardService.generateCardNumber());
			st.setString(2, "D");
			st.setInt(3, debitCard.getAccountIdAccount());
			st.setInt(4, debitCard.getCustomerIdCustomer());
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewDebitCard.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewDebitCard.", ex);
		}
	}

	public static void insertNewCreditCard(CreditCard creditCard) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall(
					"INSERT INTO card (card_number, type, account_id_acc, customer_id_cust, plafond) VALUES ( ?, ?, ?, ?, ?);");

			st.setString(1, CardService.generateCardNumber());
			st.setString(2, "C");
			st.setInt(3, creditCard.getAccountIdAccount());
			st.setInt(4, creditCard.getCustomerIdCustomer());
			st.setDouble(5, creditCard.getPlafond());
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewCreditCard.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewCreditCard.", ex);
		}

	}

	public static void deleteCard(Card card) throws DAOException {

		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("DELETE FROM card WHERE id_card = " + card.getIdCard() + ";");

			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteCard.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteCard.", ex);
		}
	}

	// TODO: ATUALIZAR METODOS DOS CARTOES!
	public static List<CreditCard> fillCreditCard(ResultSet rs, Customer customer) throws SQLException {
		// VOID OR LIST?
		if (rs.getString("type").equalsIgnoreCase("c")) {
			creditCard = new CreditCard();
			creditCard.setIdCard(rs.getInt("id_card"));
			creditCard.setCardNumber(rs.getString("card_number"));
			creditCard.setPassword(rs.getString("password"));
			creditCard.setCustomer(customer);
			creditCard.setAccountIdAccount(rs.getInt("id_acc"));
			creditCard.setCustomerIdCustomer(rs.getInt("id_cust"));
			creditCard.setPlafond(rs.getDouble("plafond"));
			creditCard.setIsFirstUse(rs.getBoolean("is_first_use"));
			if (!creditCardList.contains(creditCard)) {
				creditCardList.add(creditCard);
			}
		}
		return creditCardList;
	}

	public static List<DebitCard> fillDebitCard(ResultSet rs, Customer customer) throws SQLException {
		// VOID OR LIST?
		if (rs.getString("type").equalsIgnoreCase("d")) {
			debitCard = new DebitCard();
			debitCard.setIdCard(rs.getInt("id_card"));
			debitCard.setCardNumber(rs.getString("card_number"));
			debitCard.setPassword(rs.getString("password"));
			debitCard.setCustomer(customer);
			debitCard.setAccountIdAccount(rs.getInt("id_acc"));
			debitCard.setCustomerIdCustomer(rs.getInt("id_cust"));
			debitCard.setIsFirstUse(rs.getBoolean("is_first_use"));
			if (!debitCardList.contains(debitCard)) {
				debitCardList.add(debitCard);
			}
		}
		return debitCardList;

	}

	private static CreditCard fillCreditCard(ResultSet rs) throws SQLException {
			creditCard = new CreditCard();
			creditCard.setIdCard(rs.getInt("id_card"));
			creditCard.setCardNumber(rs.getString("card_number"));
			creditCard.setPassword(rs.getString("password"));
			creditCard.setIsFirstUse(rs.getBoolean("is_first_use"));
		return creditCard;
	}

	private static DebitCard fillDebitCard(ResultSet rs) throws SQLException {
		debitCard = new DebitCard();
		debitCard.setIdCard(rs.getInt("id_card"));
		debitCard.setCardNumber(rs.getString("card_number"));
		debitCard.setPassword(rs.getString("password"));
		debitCard.setIsFirstUse(rs.getBoolean("is_first_use"));
		return debitCard;

	}

	public static void clearCardList() {
		debitCardList.clear();
		creditCardList.clear();
	}

	public static CreditCard getCreditCardByCardNumber(String cardNumber) throws DAOException {
		CreditCard creditCard = null;
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT id_card, card_number, password, type, is_first_use FROM card "
					+ "WHERE card_number = " + cardNumber + ";");

			if (st.execute()) {
				ResultSet rs = st.getResultSet();

				if (rs.next()) {
					if (rs.getString("type").equalsIgnoreCase("C")) {
						creditCard = fillCreditCard(rs);
					}
				}
			}
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getCardByCardNumber.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getCardByCardNumber.", ex);
		}
		return creditCard;
	}

	public static DebitCard getDebitCardByCardNumber(String cardNumber) throws DAOException {
		DebitCard debitCard = null;
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT id_card, card_number, password, type, is_first_use FROM card "
					+ "WHERE card_number = " + cardNumber + ";");

			if (st.execute()) {
				ResultSet rs = st.getResultSet();

				if (rs.next()) {
					if (rs.getString("type").equalsIgnoreCase("D")) {
						debitCard = fillDebitCard(rs);
					}
				}
			}
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getCardByCardNumber.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getCardByCardNumber.", ex);
		}
		return debitCard;
	}

	public static void updatePassword(String cardNumber, String newPassword) throws DAOException {

		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("UPDATE card SET password = " + newPassword + ", "
					+ "is_first_use = 0 WHERE card_number = \"" + cardNumber + "\";");

			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updatePassword.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updatePassword.", ex);
		}
	}

}
