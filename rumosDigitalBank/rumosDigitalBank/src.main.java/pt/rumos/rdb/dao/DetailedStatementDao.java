package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.DetailedStatement;
import pt.rumos.rdb.util.JdbcConnection;

public class DetailedStatementDao {

	private static final Logger LOGGER = Logger.getLogger(DetailedStatementDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;

	public static void insertInDetailedStatement(Integer idCard, String descricao, Double amount) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("INSERT INTO detailed_statement (id_credit_card_bill, descricao, amount, date) "
					+ "VALUES (?, ?, ?, NOW());");
			st.setInt(1, idCard);
			st.setString(2, descricao);
			st.setDouble(3, amount);

			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewDetailedStatement.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewDetailedStatement.", ex);
		}

	}

	public static List<DetailedStatement> getMonthlyDetailedStatement(Integer idCard, Integer month) throws DAOException {
		List<DetailedStatement> detailedStatementList = new ArrayList<DetailedStatement>();
		
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT id_credit_card_bill, card_id_card, descricao, "
					+ "amount, date FROM detailed_statement WHERE card_id_card = " + idCard + ";");

			if (st.execute()) {
				rs = st.getResultSet();
				while (rs.next()) {
					detailedStatementList.add(fillNewDetailedStatement(rs));
				}
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getDetailedStatement.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getDetailedStatement.", ex);
		}

		return detailedStatementList;

	}

	private static DetailedStatement fillNewDetailedStatement(ResultSet rs) throws SQLException {

		DetailedStatement detailedStatement = new DetailedStatement();

		detailedStatement.setIdCreditCarBill(rs.getInt("id_credit_card_bill"));
		detailedStatement.setCardIdcard(rs.getInt("card_id_card"));
		detailedStatement.setAmount(rs.getDouble("amount"));
		detailedStatement.setDescricao(rs.getString("descricao"));
		detailedStatement.setDate(convertToLocalDateViaSqlDate(rs.getDate("date")));

		return detailedStatement;

	}

	public static LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
		return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
	}

}
