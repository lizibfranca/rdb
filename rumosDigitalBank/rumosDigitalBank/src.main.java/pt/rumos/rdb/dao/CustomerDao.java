package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.service.AdressService;
import pt.rumos.rdb.util.JdbcConnection;

public class CustomerDao {

	private static final Logger LOGGER = Logger.getLogger(CustomerDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;
	private static Customer customer = new Customer();
	private static List<Customer> customerList = new ArrayList<Customer>();


	public static Customer getCustomerByNif(String nif) throws DAOException {

		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection
					.prepareCall("SELECT customer.id_cust, customer.nif, customer.full_name, customer.birthdate, customer.occupation, "
							+ "customer.email, customer.home_phone, customer.cellphone, customer.is_owner, customer.password, "
							+ "adress.id_adress, adress.street, adress.city, adress.state_province, "
							+ "adress.zip_postal_code, adress.country_region FROM customer INNER JOIN adress "
							+ "WHERE customer.nif = " + nif + " AND adress.customer_id_cust =  customer.id_cust;");

			if (st.execute()) {
				rs = st.getResultSet();

				if (rs.next()) {
					fillCustomer(rs);
				} else {
					customer = null;
				}
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getCustomerByNif.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getCustomerByNif.", ex);
		}
		return customer;
	}

	public static List<Customer> fillCustomer(ResultSet rs) throws SQLException {
		customer = new Customer();
		customer.setIdCustomer(rs.getInt("id_cust"));
		customer.setNif(rs.getString("nif"));
		customer.setName(rs.getString("full_name"));
		Date date = rs.getDate("birthdate");
		if (date != null) {
			customer.setBirthdate(convertToLocalDateViaSqlDate(date));
		}
		customer.setOccupation(rs.getString("occupation"));
		customer.setEmail(rs.getString("email"));
		customer.setTelephone(rs.getString("home_phone"));
		customer.setCellphone(rs.getString("cellphone"));
		customer.setAdress(AdressService.setAdress(rs));
		customer.setTheOwner(rs.getBoolean("is_owner"));
		customer.setPassword(rs.getString("password"));

		if (!(customerList.contains(customer))) {
			customerList.add(customer);
		}
		return customerList;
	}

	public static void insertNewCustomer(Customer customer) throws DAOException {
		try {

			String sql = "INSERT INTO customer (nif, full_name, birthdate, occupation, email, home_phone, cellphone, is_owner, password) "
					+ "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?);";

			connection = JdbcConnection.getConnectionMySql();

			PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, customer.getNif());
			ps.setString(2, customer.getName());
			ps.setDate(3, java.sql.Date.valueOf(customer.getBirthdate()));
			ps.setString(4, customer.getOccupation());
			ps.setString(5, customer.getEmail());
			ps.setString(6, customer.getTelephone());
			ps.setString(7, customer.getCellphone());
			ps.setBoolean(8, customer.isTheOwner());
			ps.setString(9, customer.getPassword());

			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				int lastId = rs.getInt(1);
				customer.setIdCustomer(lastId);
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewCustomer.", ex);
		}

	}

	public static void updateCustomer(Customer customer) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("UPDATE customer SET full_name= ?, occupation= ?, email= ?, "
					+ "home_phone= ?, cellphone= ?, password= ? WHERE id_cust = " + customer.getIdCustomer() + ";");
			
			st.setString(1, customer.getName());
			st.setString(2, customer.getOccupation());
			st.setString(3, customer.getEmail());
			st.setString(4, customer.getTelephone());
			st.setString(5, customer.getCellphone());
			st.setString(6, customer.getPassword());
			
			st.execute();
			
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updateCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updateCustomer.", ex);
		}

	}

	public static void deleteCustomer(Customer customer) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM customer WHERE id_cust = " + customer.getIdCustomer() + ";");
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteCustomer.", ex);
		}

	}

	public static void clearCustomerList() {
		customerList.clear();
	}

	public static LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
		return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
	}
}
