package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.TransactionHistory;
import pt.rumos.rdb.model.WithdrawalControl;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.util.JdbcConnection;
import pt.rumos.rdb.view.RdbView;

public class WithdrawalControlDao {

	private static final Logger LOGGER = Logger.getLogger(WithdrawalControlDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;

	public static WithdrawalControl getTotalWithdrawalDay(Integer idAccount) throws DAOException {
		WithdrawalControl withdrawalControl = null;
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT id_withdrawal_control, account_id_acc, "
					+ "sum_withdrawal_day, date "
					+ "FROM withdrawal_control WHERE account_id_acc = " + idAccount + ";");

			if (st.execute()) {
				rs = st.getResultSet();
				if (rs.next()) {
					withdrawalControl = fillNewWithdrawalControl(rs);
				}
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updateBalance.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updateBalance.", ex);
		}

		return withdrawalControl;

	}
	
	public static void updateTotalWithdrawal(Integer idAccount, Double amount) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("UPDATE withdrawal_control SET sum_withdrawal_day= ? "
					+ "WHERE account_id_acc = \"" + idAccount + "\";");
			
			st.setDouble(1, amount);
			
			st.execute();
			
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updateCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updateCustomer.", ex);
		}

	}
	
	public static void resetTotalWithdrawal(Integer idAccount, Double amount) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("UPDATE withdrawal_control SET sum_withdrawal_day= ?, "
					+ "date = NOW() WHERE account_id_acc = \"" + idAccount + "\";");
			
			st.setDouble(1, amount);
			
			st.execute();
			
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updateCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updateCustomer.", ex);
		}

	}

	private static WithdrawalControl fillNewWithdrawalControl(ResultSet rs) throws SQLException {

		WithdrawalControl withdrawalControl = new WithdrawalControl();

		withdrawalControl.setIdWithdrawalControl(rs.getInt("id_withdrawal_control"));
		withdrawalControl.setIdAccount(rs.getInt("account_id_acc"));
		withdrawalControl.setSumWithdrawalDay(rs.getDouble("sum_withdrawal_day"));
		withdrawalControl.setDate(convertToLocalDateViaSqlDate(rs.getDate("date")));

		return withdrawalControl;

	}

	public static LocalDate convertToLocalDateViaSqlDate(Date dateToConvert) {
		return new java.sql.Date(dateToConvert.getTime()).toLocalDate();
	}

	public static void insertNewWithdralControl(Integer idAccount) throws DAOException {
		try {

			String sql = "INSERT INTO withdrawal_control (account_id_acc, sum_withdrawal_day, "
					+ "date) VALUES ( ?, 0, NOW());";

			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			PreparedStatement ps = connection.prepareStatement(sql);

			ps.setInt(1,idAccount);
			ps.executeUpdate();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewWithdralControl.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewWithdralControl.", ex);
		}
	}
	
	public static void deleteWithdralControl(int idAcc) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM withdrawal_control WHERE account_id_acc = " 
			+ idAcc + ";");
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteWithdralControl.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteWithdralControl.", ex);
		}
	}

}
