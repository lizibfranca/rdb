package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.Adress;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.util.JdbcConnection;

public class AdressDao {

	private static final Logger LOGGER = Logger.getLogger(AdressDao.class.getName()); // TODO: CORRIGIR
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;

	public static void insertCustomersAdress(Customer customer) throws DAOException {
		// OK
		try {

			String sql = "INSERT INTO adress (street, city, state_province, zip_postal_code, country_region, "
					+ "customer_id_cust) VALUES ( ?, ?, ?, ?, ?, ?);";

			connection = JdbcConnection.getConnectionMySql();

			PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, customer.getAdress().getStreet());
			ps.setString(2, customer.getAdress().getCity());
			ps.setString(3, customer.getAdress().getState());
			ps.setString(4, customer.getAdress().getPostalCode());
			ps.setString(5, customer.getAdress().getCountry());
			ps.setInt(6, customer.getIdCustomer());

			ps.executeUpdate();
			rs = ps.getGeneratedKeys();
			if (rs.next()) {
				int lastId = rs.getInt(1);
				customer.getAdress().setIdAdress(lastId); 
			}
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertCustomersAdress.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertCustomersAdress.", ex);
		}

	}

	public static void deleteAdress(Adress adress) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM adress WHERE id_adress = " + adress.getIdAdress() + ";");
			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteAdress.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteAdress.", ex);
		}

	}

}
