package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.model.TransactionHistory;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.service.CustomerService;
import pt.rumos.rdb.util.JdbcConnection;

public class BankAccountDao {

	private static final Logger LOGGER = Logger.getLogger(BankAccountDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;
	private static BankAccount account = new BankAccount();
	private static List<BankAccount> accountList = new ArrayList<BankAccount>();

	public static BankAccount getAccountByAccountNumber(String accountNumber) throws DAOException {
		// OK
		try {
			accountList.clear();
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("SELECT acc.id_acc, acc.account_number, acc.balance, "
					+ "cus.id_cust, cus.nif, cus.full_name, cus.birthdate, cus.occupation, cus.email, cus.home_phone, "
					+ "cus.cellphone, cus.is_owner, cus.password, ad.id_adress, ad.street, ad.city, ad.state_province, ad.zip_postal_code, ad.country_region, "
					+ "k.id_card, k.card_number, k.type, k.password, k.account_id_acc, k.customer_id_cust, k.plafond, k.is_first_use "
					+ "FROM customer cus INNER JOIN adress ad ON cus.id_cust = ad.customer_id_cust "
					+ "INNER JOIN account_has_customer has ON cus.id_cust=has.customer_id_cust "
					+ "LEFT JOIN account acc ON has.account_id_acc=acc.id_acc "
					+ "LEFT JOIN card k ON (acc.id_acc=k.account_id_acc AND k.customer_id_cust = cus.id_cust) "
					+ "WHERE acc.account_number= " + accountNumber + ";");

			if (st.execute()) {
				rs = st.getResultSet();

				while (rs.next()) {
					// TODO: O FILL CUSTOMER N RETORNA O OBJETO!
					fillBankAccount(rs);// TODO: ATEN��O A ESTA LINHA
					setAccountsCustomer(CustomerDao.fillCustomer(rs));
					if ((rs.getString("card_number") != null)) {
						int index = account.getCustomers().size() - 1;
						setAccountsDebitCards(CardDao.fillDebitCard(rs, account.getCustomers().get(index)));
						setAccountsCreditCards(CardDao.fillCreditCard(rs, account.getCustomers().get(index)));
					}
				}
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getAccountByAccountNumber.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getAccountByAccountNumber.", ex);
		}
		if (!(accountList.isEmpty())) {
			return accountList.get(0);
		} else {
			return null;
		}
	}

	public static List<BankAccount> getAccountByNif(String nif) throws DAOException {
		// TODO: REFAZER P RETORNAR APENAS O NIF INSERIDO! (?)
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("SELECT DISTINCT account_number FROM customer cus "
					+ "INNER JOIN account_has_customer has ON cus.id_cust=has.customer_id_cust "
					+ "LEFT JOIN account acc ON has.account_id_acc=acc.id_acc " + "WHERE cus.nif= " + nif + ";");
			List<String> accountNumberList = new ArrayList<String>();
			if (st.execute()) {
				rs = st.getResultSet();

				while (rs.next()) {
					accountNumberList.add(rs.getString("account_number"));
				}
				if (accountNumberList.isEmpty()) {
					accountList.clear();
				}
				for (String accountNumber : accountNumberList) {
					// TODO: TESTAR COM MAIS DE 1 CONTA P NIF
					getAccountByAccountNumber(accountNumber);
				}

			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getAccountByNif.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getAccountByNif.", ex);
		}
		return accountList; //TODO: verificar se esta lista � utilizada
	}
	
	public static BankAccount getAccountByCardNumber(String cardNumber) throws DAOException {
		String accNumber = null;
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("SELECT account_number FROM account acc " 
					+ "INNER JOIN card k ON acc.id_acc = k.account_id_acc " 
					+ "WHERE k.card_number =  " + cardNumber + ";");
			
			if (st.execute()) {
				rs = st.getResultSet();

				while (rs.next()) {
					accNumber = rs.getString("account_number");
				}
				if (accNumber != null) {
					getAccountByAccountNumber(accNumber);
				}

			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getAccountByCardNumber.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getAccountByCardNumber.", ex);
		}
		if (!(accountList.isEmpty())) {
			return accountList.get(0);
		} else {
			return null;
		}
	}


	public static List<BankAccount> getAllAccounts() throws DAOException {
		accountList.clear();
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);
			
			st = connection.prepareCall("SELECT acc.id_acc, acc.account_number, acc.balance, "
					+ "cus.id_cust, cus.nif, cus.full_name, cus.birthdate, cus.occupation, cus.email, cus.home_phone, "
					+ "cus.cellphone, cus.is_owner, cus.password, ad.id_adress, ad.street, ad.city, ad.state_province, ad.zip_postal_code, ad.country_region, "
					+ "k.id_card, k.card_number, k.type, k.password, k.account_id_acc, k.customer_id_cust, k.plafond, k.is_first_use "
					+ "FROM customer cus INNER JOIN adress ad ON cus.id_cust = ad.customer_id_cust "
					+ "INNER JOIN account_has_customer has ON cus.id_cust=has.customer_id_cust "
					+ "LEFT JOIN account acc ON has.account_id_acc=acc.id_acc "
					+ "LEFT JOIN card k ON (acc.id_acc=k.account_id_acc AND k.customer_id_cust = cus.id_cust)"
					+ "ORDER BY acc.account_number; ");

			if (st.execute()) {
				rs = st.getResultSet();

				while (rs.next()) {
					// TODO: O FILL CUSTOMER N RETORNA O OBJETO!
					fillBankAccount(rs);// TODO: ATEN��O A ESTA LINHA
					setAccountsCustomer(CustomerDao.fillCustomer(rs));
					if ((rs.getString("card_number") != null)) {
						int index = account.getCustomers().size() - 1;
						setAccountsDebitCards(CardDao.fillDebitCard(rs, account.getCustomers().get(index)));
						setAccountsCreditCards(CardDao.fillCreditCard(rs, account.getCustomers().get(index)));
					}

					// accountList.add(account);
				}

				// fillBankAccount(rs);
			}
			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getAllAccounts.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getAllAccounts.", ex);
		}

		return accountList;
	}

	public static void insertNewAccount(BankAccount account) throws DAOException {
		try {

			String sql = "INSERT INTO account (account_number, balance) VALUES ( ?, ?);";

			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			PreparedStatement ps = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

			ps.setString(1, account.getAccountNumber());
			ps.setDouble(2, account.getBalance());
			ps.executeUpdate();

			rs = ps.getGeneratedKeys();

			if (rs.next()) {
				int lastId = rs.getInt(1);
				account.setIdAccount(lastId);
			}

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertNewAccount.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertNewAccount.", ex);
		}

	}

	public static Integer selectMaxAccountId() throws DAOException {
		// OK
		int maxIdAcc = -1;
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("SELECT MAX(id_acc) FROM account;");

			if (st.execute()) {
				rs = st.getResultSet();
				rs.next();
				maxIdAcc = rs.getInt("MAX(id_acc)");
				// AdressDao.insertCustomersAdress(rs.getInt("MAX(id_cust)"));
			}
			JdbcConnection.closeConnection(connection);
		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing selectMaxAccountId.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing selectMaxAccountId.", ex);
		}
		return maxIdAcc;
	}

	public static Integer getAccountId(String accountNumber) throws DAOException {
		int idAcc = -1;
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("SELECT id_acc FROM account WHERE account_number = " + accountNumber + ";");

			if (st.execute()) {
				rs = st.getResultSet();
				rs.next();
				idAcc = rs.getInt("id_acc");
			}
			JdbcConnection.closeConnection(connection);
		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getAccountId.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getAccountId.", ex);
		}
		return idAcc;
	}

	public static void deleteAccount(BankAccount account) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("DELETE FROM account WHERE id_acc = " + account.getIdAccount() + ";");

			st.execute();
			JdbcConnection.closeConnection(connection);
		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteAccount.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteAccount.", ex);
		}

	}

	public static double getBalance(String accountNumber) throws DAOException {
		Double balance = null;
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("SELECT balance FROM account WHERE account_number = " + accountNumber + ";");

			if (st.execute()) {
				rs = st.getResultSet();
				if (rs.next()) {
					balance = rs.getDouble("balance");
				}
			}
			JdbcConnection.closeConnection(connection);
		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing getBalance.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing getBalance.", ex);
		}
		return balance;

	}

	public static void updateBalance(String accountNumber, Double amount) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			// connection.setAutoCommit(false);

			st = connection.prepareCall("UPDATE account SET balance = balance + " + amount + "WHERE account_number = \""
					+ accountNumber + "\";");

			st.execute();
			JdbcConnection.closeConnection(connection);
		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing updateBalance.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing updateBalance.", ex);
		}

	}

	private static BankAccount fillBankAccount(ResultSet rs) throws SQLException {
		// TODO: VRIFICAR
		BankAccount newAccount = new BankAccount();
		newAccount.setIdAccount(rs.getInt("id_acc"));
		newAccount.setAccountNumber(rs.getString("account_number"));
		newAccount.setBalance(rs.getDouble("balance"));
		if (!(accountList.contains(newAccount))) {
			account = newAccount;
			accountList.add(account);
			CustomerDao.clearCustomerList();
			CardDao.clearCardList();
		}

		return account; //precisa retornar a conta aqui?
	}
	
	private static void setAccountsCustomer(List<Customer> customerList) throws SQLException {

		for (Customer customer : customerList) {
			if (!account.getCustomers().contains(customer)) {
				account.setCustomers(customer);
			}
		}
	}

	private static void setAccountsCreditCards(List<CreditCard> creditCardList) {
		if (!creditCardList.isEmpty()) {
			for (CreditCard creditCard : creditCardList) {
				if (!account.getCreditCardList().contains(creditCard)) {
					account.setCreditCard(creditCard);
				}
			}
		}
	}

	private static void setAccountsDebitCards(List<DebitCard> debitCardList) {
		if (!debitCardList.isEmpty()) {
			for (DebitCard debitCard : debitCardList) {
				if (!account.getDebitCardList().contains(debitCard)) {
					account.setDebitCard(debitCard);
				}
			}
		}
	}
	
}
