package pt.rumos.rdb.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.util.JdbcConnection;

public class AccountHasCustomerDao {
	
	private static final Logger LOGGER = Logger.getLogger(AccountHasCustomerDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	
	public static void insertRelationshipAccountCustomer(BankAccount account, Customer customer) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();
			
			//connection.setAutoCommit(false);

			st = connection.prepareCall("INSERT INTO account_has_customer " + 
					"(account_id_acc, customer_id_cust) VALUES (?, ?);" );

			st.setInt(1, account.getIdAccount());
			st.setInt(2, customer.getIdCustomer());
			if (st.execute())
				System.out.println("insertNewAccount performed succesfully.");

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing insertRelationshipAccountCustomer.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing insertRelationshipAccountCustomer.", ex);
		}

	}

	public static void deleteRelationship(int idAcc, int idCust) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM account_has_customer " + 
					"WHERE account_id_acc = " + idAcc + " AND customer_id_cust = " + idCust + ";" );

			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteRelationship idAcc idCust.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteRelationship idAcc idCust.", ex);
		}
		
	}

	public static void deleteRelationship(int idAcc) throws DAOException {
		try {
			connection = JdbcConnection.getConnectionMySql();

			st = connection.prepareCall("DELETE FROM account_has_customer " + 
					"WHERE account_id_acc = " + idAcc + ";" );

			st.execute();

			JdbcConnection.closeConnection(connection);

		} catch (SQLException | ClassNotFoundException ex) {
			LOGGER.log(Level.SEVERE, "Erro whiling processing deleteRelationship idAcc.", ex);
			JdbcConnection.closeConnection(connection);
			throw new DAOException("Erro whiling processing deleteRelationship idAcc.", ex);
		}
		
		
	}

}
