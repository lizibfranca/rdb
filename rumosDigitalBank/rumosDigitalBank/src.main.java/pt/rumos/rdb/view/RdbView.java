package pt.rumos.rdb.view;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.Scanner;

public class RdbView {

	private static Scanner scan = new Scanner(System.in);

	public static void outputRdb() {
		System.out.println("\n Welcome to Rumos Digital Bank\r\n" 
				+ "         ______    ______     ______\r\n"
				+ "	|   _  \\  |   _  \\   |   _  \\   \r\n" 
				+ "	|  |_)  | |  | \\  \\  |  |_) /		\r\n"
				+ "	|      /  |  |  |  | |   _  <\r\n" 
				+ "	|  |\\  \\  |  |_/  /  |  |_)  \\\r\n"
				+ "	|__| \\__\\ |______/   |_______/\r\n\n");
	}

	public static String inputNif() {
		String nif;
		do {
			System.out.println("Enter the Nif: ");
			nif = scan.nextLine();
		} while (nif.isBlank());

		return nif;
	}

	public static String inputFullName() {
		String name;
		do {
			System.out.println("Enter the full name: ");
			name = scan.nextLine();
		} while (name.isBlank());

		return name;
	}

	public static LocalDate inputBirthdate() {
		LocalDate birthdate = null;
		do {
			try {
				System.out.println("Enter the birthdate: (aaaa-mm-dd)");
				birthdate = LocalDate.parse(scan.nextLine());

			} catch (DateTimeException e) {
				System.out.println("Date format should be: aaaa-mm-dd");
			}
		} while (birthdate == null);

		return birthdate;
	}

	public static String inputOccupation() {
		System.out.println("Enter the occupation: ");
		return scan.nextLine();
	}

	public static String inputEmail() {
		System.out.println("Enter the e-mail: ");
		return scan.nextLine();
	}

	public static String inputTelephone() {
		String telephone;
		do {
			System.out.println("Enter the telephone: ");
			telephone = scan.nextLine();
		} while (!telephone.isBlank() & telephone.length() < 9);
		return telephone;
	}

	public static String inputCellphone() {
		String cellphone;
		do {
			System.out.println("Enter the cellphone: ");
			cellphone = scan.nextLine();
		} while (!cellphone.isBlank() & cellphone.length() < 9);
		return cellphone;
	}

	public static String inputStreet() {
		System.out.println("Enter the street: ");
		return scan.nextLine();
	}

	public static String inputCity() {
		System.out.println("Enter the city: ");
		return scan.nextLine();
	}

	public static String inputState() {
		System.out.println("Enter the state: ");
		return scan.nextLine();
	}

	public static String inputPostalCode() {
		System.out.println("Enter the postal code: ");
		return scan.nextLine();
	}

	public static String inputCountry() {
		System.out.println("Enter the country: ");
		return scan.nextLine();
	}

	public static double inputFirstDeposit() {
		// TODO: TRATAR EXCEPTION DO PONTO NA CASA DECIMAL
		System.out.println("Enter the amount of the first deposit: (Min value: 50�)");
		return Double.valueOf(scan.nextLine());
	}

	public static double inputAmount() {
		System.out.println("Enter the amount: ");
		// TODO: TRATAR EXCEPTION DO PONTO NA CASA DECIMAL
		return Double.valueOf(scan.nextLine());
	}

	public static boolean inputAddNewDebitCard() {
		String input = "0";
		do {
		System.out.println("Do you want add a Debit Card? (Y/N)");
		input = scan.nextLine();
		} while (!(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("n")));
		if (input.equalsIgnoreCase("y")) {
			return true;
		}
		return false;
	}

	public static boolean inputAddNewCreditCard() {
		String input = "0";
		do {
		System.out.println("Do you want add a Credit Card? (Y/N)");
		input = scan.nextLine();
		} while (!(input.equalsIgnoreCase("y") || input.equalsIgnoreCase("n")));
		if (input.equalsIgnoreCase("y")) {
			return true;
		}
		return false;
	}

	public static String inputAccountNumber() {
		System.out.println("Enter the account number: ");
		return (scan.nextLine());
	}

	public static String inputTargetAccountNumber() {
		System.out.println("Enter the target account number: ");
		return (scan.nextLine());
	}

	public static int inputAccountManager() {
		System.out.println("Enter 1 for balance;");
		System.out.println("Enter 2 for bank transfer; ");
		System.out.println("Enter 3 for transactions history; ");
		System.out.println("Enter 4 for new debit card;");
		System.out.println("Enter 5 for new credit card;");
		System.out.println("Enter 6 to cancel a debit card;");
		System.out.println("Enter 7 to cancel a credit card;");
		System.out.println("Enter 0 (zero) to exit. ");
		return Integer.valueOf(scan.nextLine()) ;
	}
	
	public static String inputCardNumber() {
		System.out.println("Enter your card Number: ");
		return scan.nextLine();
	}
	
	public static int inputMonth() {
		String month;
		do {
		System.out.println("Enter the month: (Format: mm)");
		month = scan.nextLine();
		} while (!month.matches("[0-12]+"));
		return Integer.valueOf(month);
	}

	public static void outputBalance(Double balance) {
		System.out.println("Your balance is: " + balance + " �");

	}

	public static void outputDoneSuccessfully() {
		System.out.println("Operation performed successfully!");

	}

	public static void outputProblem() {
		System.out.println("Problem while performing the operation.");

	}

	public static void outputInvalidOption() {
		System.out.println("Invalid option");

	}

	public static void outputIncorrectPass() {
		System.out.println("Incorrect password.");
	}

	public static void outputNifNotFound() {
		System.out.println("NIF not found.");
	}

	public static void outputAccountNotFound() {
		System.out.println("Sorry, account not found.");
	}

	public static int outputRdbManegement() {
		System.out.println("WELCOME TO THE RDB MANEGEMENT!");
		System.out.println("***MAIN MENU***");
		System.out.println("Choose an option: ");
		System.out.println("1 - New Account");
		System.out.println("2 - New customer (co-owner)");
		System.out.println("3 - Display account by nif");
		System.out.println("4 - List all accounts");
		System.out.println("5 - Delete Customer");
		System.out.println("0 - Exit");
		return Integer.valueOf(scan.nextLine());
	}
	
	public static int outputCustomerManegement() {
		System.out.println("WELCOME TO THE CUSTOMER'S MANEGEMENT!");
		System.out.println("***MAIN MENU***");
		System.out.println("Choose an option: ");
		System.out.println("1 - Edit customer");
		System.out.println("2 - Account manager");
		System.out.println("0 - Exit");
		return Integer.valueOf(scan.nextLine());
	}
	
	public static String inputNewPassword() {
		String password;
		do {
			System.out.println("Enter your new password: (4 digits, only numbers)");
			password = scan.nextLine();
		} while (password.isBlank() || !(password.matches("[0-9]+") && password.strip().length() == 4));
		return password;
	}

	public static int outputEditCustomer() {
		System.out.println("Choose an option: ");
		System.out.println("1 - Edit name ");
		System.out.println("2 - Change password ");
		System.out.println("3 - Edit telephone ");
		System.out.println("4 - Edit cellphone");
		System.out.println("5 - Edit e-mail ");
		System.out.println("6 - Edit occupation ");
		System.out.println("0 - Exit");
		return Integer.valueOf(scan.nextLine());
	}

	public static String inputPassword() {
		System.out.println("Enter your password: ");
		return scan.nextLine();
	}

	public static int outputAtmOptions(String cardType) {
		System.out.println("RDB ATM \n"  
			+ "Choose an option: \n"
			+ "1 - Balance \n"
			+ "2 - Deposit \n"
			+ "3 - Transference \n"
			+ "4 - Withdraw Account\n"
			+ "5 - Transactions history\n"
			+ "6 - Update password\n"
			+ "7 - Pay credit card bill"); //TODO:
			if (cardType.equalsIgnoreCase("C")) {
				System.out.println("8 - Withdraw plafond\n"
				+ "9 - Pay plafond");
			}
			System.out.println( "0 - Exit\n");
		
		return Integer.valueOf(scan.nextLine());
		
	}

	public static void outputInsufficientFunds() {
		System.out.println("Insufficient funds.");
	}

	public static void outputDailyLimitExceeded() {
		System.out.println("Daily limit exceeded. \n"
				+ "Limit per withdral: 200�; \n"
				+ "Limit per day: 400�.");
	}
}
