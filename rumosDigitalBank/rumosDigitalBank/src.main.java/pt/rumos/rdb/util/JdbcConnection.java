package pt.rumos.rdb.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class JdbcConnection {
								
	private static final String url = "jdbc:mysql://localhost:3306/"; 
	private static final String database = "rumosdigitalbank";
	private static final String finalUrl = url + database;
	private static final String username = "root";
	private static final String password = "12345";

	private static final Logger LOGGER = Logger.getLogger(JdbcConnection.class.getName());
	
	public static Connection getConnectionMySql() throws ClassNotFoundException, SQLException {
		
		//Class.forName("com.mysql.jdbc.driver");
														//TROCAR URL
		Connection conn = DriverManager.getConnection(finalUrl, username, password);

		return conn;

	}

	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {

				connection.close();
				if (LOGGER.isLoggable(Level.FINER)) {
					LOGGER.log(Level.FINER, "Disconnected JDBC connection from data source.");
				}
			} catch (SQLException e) {
				LOGGER.log(Level.SEVERE, "Error cleaning up JDBC connection from data source.", e);
			}
		}

	}

}
