package pt.rumos.rdb.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import pt.rumos.rdb.dao.AccountHasCustomerDao;
import pt.rumos.rdb.dao.AdressDao;
import pt.rumos.rdb.dao.BankAccountDao;
import pt.rumos.rdb.dao.CardDao;
import pt.rumos.rdb.dao.CustomerDao;
import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.service.CustomerService;
import pt.rumos.rdb.view.RdbView;

public class Main {

	private static Scanner scan = new Scanner(System.in);
	private static final Logger LOGGER = Logger.getLogger(CustomerDao.class.getName());
	private static Connection connection = null;
	private static CallableStatement st;
	private static ResultSet rs;
	private static Customer customer;
	private static List<Customer> customerList = new ArrayList<>();
	private static BankAccount account;
	private static List<DebitCard> debitCardList = new ArrayList<>();
	private static List<CreditCard> creditCardList = new ArrayList<>();
	private static List<BankAccount> accountList = new ArrayList<>();
	
	/* 
	 * INSERIR NEWCUSTOMER EM CONTA J� EXISTENTE
	 * VER COMO FAZER O COMMIT E ROLLBACK
	 * TRATAR AS EXCEPTIONSSSSS
	 * */

	public static void main(String[] args) throws DAOException {
			
			Integer month = LocalDate.now().getMonthValue();
			
			System.out.println(month);
			
		}

}
