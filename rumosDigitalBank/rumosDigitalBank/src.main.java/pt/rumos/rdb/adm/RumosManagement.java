package pt.rumos.rdb.adm;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CustomerService;
import pt.rumos.rdb.view.RdbView;

public class RumosManagement {

	// TODO: PROBLEMA COM NEXTLINE();

	private int option;

	public void run() throws DAOException {
		do {
			option = RdbView.outputRdbManegement();
			switch (option) {
			case 0: {
				break;
			}
			// *************************** MENU 1
			case 1: {
				// TODO: FALTAM AS RESTRIÇÕES
				// TODO: CRIAR E PREENCHER OBJETOS AQUI!
				try {
					BankAccountService.registerNewAccount();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
				break;

			// *************************** MENU 2
			case 2: {
				try {
					CustomerService.newCustomerCoOwner();
				} catch (Exception e) {
					e.printStackTrace();
				}
				break;
			}

			// *************************** MENU 2
		/*	case 3: {
				CustomerService.editCustomer();
				break;
			} */

			// *************************** MENU 3
			case 3: {
				BankAccountService.displayAccountByNif();
				break;
			}

			// *************************** MENU 4
			case 4: {
				BankAccountService.displayAllAccounts();
				break;
			}

			// *************************** MENU 5
			case 5: {
				CustomerService.deleteCustomer();
				break;
			}

			// *************************** MENU 6
		/*	case 7: {
				BankAccountService.accountManager();
				break;
			} */

			default:
				RdbView.outputInvalidOption();
				break;
			}
		} while (option != 0);

	}

}
