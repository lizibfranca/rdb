package pt.rumos.rdb.adm;

import pt.rumos.rdb.exception.DAOException;
import pt.rumos.rdb.model.BankAccount;
import pt.rumos.rdb.model.CreditCard;
import pt.rumos.rdb.model.Customer;
import pt.rumos.rdb.model.DebitCard;
import pt.rumos.rdb.service.BankAccountService;
import pt.rumos.rdb.service.CardService;
import pt.rumos.rdb.service.CustomerService;
import pt.rumos.rdb.view.RdbView;

public class CustomerManagement {

	private static BankAccount account;

	public void run() throws DAOException {
		int option;
		do {
			option = RdbView.outputCustomerManegement();
			switch (option) {
			case 0:
				break;
			case 1:
				editCustomer();
				break;
			case 2:
				accountManager();
				break;
			}
		} while (option != 0);
	}

	public static void editCustomer() throws DAOException {
		if (CustomerService.isNifAlreadyRegistered()) { 
			if(CustomerService.checkCustomersPassword()) {
				
			int option = RdbView.outputEditCustomer();

				switch (option) {
				case 0: {
					break;
				}
				case 1: {
					String name= RdbView.inputFullName();
					CustomerService.setName(name);
					break;
				}
				case 2: {
					String password = RdbView.inputNewPassword();
					CustomerService.setPassword(password);
					break;
				}	
				case 3: {
					String telephone = RdbView.inputTelephone();
					CustomerService.setTelephone(telephone);
					break;
				}				
				case 4: {
					String cellphone = RdbView.inputCellphone();
					CustomerService.setCellphone(cellphone);
					break;
				}
				case 5: {
					String email = RdbView.inputEmail();
					CustomerService.setEmail(email);
					break;
				}
				case 6: {
					String occupation = RdbView.inputOccupation();
					CustomerService.setOccupation(occupation);
					break;
				}
				default:
					RdbView.outputInvalidOption();
					break;
				}
				//TODO: O UPDATE RODA MESMO NO DEFAULT
			CustomerService.updateCustomer();
		} else {
			RdbView.outputIncorrectPass();
		} 
		} else {
			RdbView.outputNifNotFound();
		}
	}

	public static void accountManager() throws DAOException {
		String accountNumber = RdbView.inputAccountNumber();
		
		account = BankAccountService.getAccountByAccountNumber(accountNumber);

		if ((account != null)) {
			String nif = RdbView.inputNif();
			Customer customer = CustomerService.getCustomerByNif(nif);
			if (account.getCustomers().contains(customer)) {
				if (CustomerService.checkCustomersPassword()) {

					int option = RdbView.inputAccountManager();

					switch (option) {
					case 0: {
						break;
					}
					case 1: {
						BankAccountService.displayBalance(accountNumber);
						break;
					}
					case 2: {
						String targetAccountNumber = RdbView.inputAccountNumber();
						Double amount = RdbView.inputAmount();
						BankAccountService.transference(accountNumber, targetAccountNumber, amount);
						break;
					}
					case 3: {
						//int month =  RdbView.inputMonth();
						BankAccountService.displayTransactionHistory(account.getIdAccount());
						break;
					}
					case 4: {
						if (account.getDebitCardList().isEmpty()) {
							CardService.registerNewDebitCard(account.getIdAccount(), customer.getIdCustomer());
							RdbView.outputDoneSuccessfully();
						} else {
							for (DebitCard debitCard : account.getDebitCardList()) {
								if (BankAccountService.checkTotalDebitCards(account)
										&& !(debitCard.getCustomer().equals(customer))) {
									CardService.registerNewDebitCard(account.getIdAccount(), customer.getIdCustomer());
									RdbView.outputDoneSuccessfully();
								} else {
									RdbView.outputProblem();
								}
							}
						}
						break;
					}
					case 5: {
						if (account.getCreditCardList().isEmpty()) {
							CardService.registerNewCreditCard(account.getIdAccount(), customer.getIdCustomer());
							RdbView.outputDoneSuccessfully();
						} else {
							for (CreditCard creditCard : account.getCreditCardList()) {
								if (BankAccountService.checkTotalCreditCards(account)
										&& !(creditCard.getCustomer().equals(customer))) {
									CardService.registerNewCreditCard(account.getIdAccount(), customer.getIdCustomer());
									RdbView.outputDoneSuccessfully();
								} else {
									RdbView.outputProblem();
								}
							}
						}
						break;
					}
					case 6: {
						CardService.cancelDebitCard(account.getDebitCardList(), account.getIdAccount(),
								customer.getIdCustomer());
						break;
					}
					case 7: {
						CardService.cancelCreditCard(account.getCreditCardList(), account.getIdAccount(),
								customer.getIdCustomer());
						break;
					}
					default:
						RdbView.outputInvalidOption();
						break;
					}

				} else {
					RdbView.outputIncorrectPass();
				}

			} else {
				RdbView.outputNifNotFound();
			}
		} else {
			RdbView.outputAccountNotFound();
		}

	}
}
