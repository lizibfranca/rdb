package pt.rumos;

import pt.rumos.rdb.RumosDigitalBank;
import pt.rumos.rdb.exception.DAOException;

public class App {

	public static void main(String[] args) {
		RumosDigitalBank rdb = new RumosDigitalBank();
		try {
			rdb.run();
		} catch (DAOException e) {
			//TODO: ERROR MESSAGE
			e.printStackTrace();
		}
	}
}
